-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-07-2021 a las 18:06:52
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- Dumping database structure for pedidos
CREATE DATABASE IF NOT EXISTS `proy_AYD2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `proy_AYD2`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID_USUARIO` int(5) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `PATERNO` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `MATERNO` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `FECHA_NACIMIENTO` varchar(30) NOT NULL,
  `CI` text COLLATE utf8_spanish_ci NOT NULL,
  `NRO_CELULAR` int(9) NOT NULL,
  `ROL` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `CORREO` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `CONTRASEÑA` varchar(20) COLLATE utf8_spanish_ci NOT NULL,	
  `ESTADO` tinyint(1) NOT NULL,
   PRIMARY KEY(`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;




-- Dumping data for table usuarios.proy_ayd2: 
INSERT INTO `usuarios` (`NOMBRE`, `PATERNO`, `MATERNO`, `FECHA_NACIMIENTO`, `CI`, `NRO_CELULAR`, `ROL`, `CORREO`, `CONTRASEÑA`, `ESTADO`) VALUES 
   ('Juan Carlos', 'Abrego', 'Lopez', '2000-02-16', '5292034LP', '71902304', 'Administrador','jc@gmail.com','cliente1234', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
