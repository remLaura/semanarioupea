<?php 
	session_start();
	require '../Conexion/conexion.php';
	$id_user=$_GET['iduser'];
 ?>
 <!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Menu de Navegación</title>
  <link rel="stylesheet" href="../CSS/styleupd.css">
</head>
<body>

  <header>
    <div class="menu">
      <img src="../logo.png" alt="">
      <nav>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre mi</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="../Procesos/close.php">Cerrar Sesion</a></li>
          </ul>
      </nav>
    </div>
  </header>
  <section>
  	<?php
  		$sql="SELECT * FROM usuarios WHERE ID_USUARIO=$id_user";
  		$result=$conexion->query($sql);
  		foreach ($result as $value) {
  			$nombres=$value['NOMBRE'];
  			$paterno=$value['PATERNO'];
  			$materno=$value['MATERNO'];
  			$nacimiento=$value['FECHA_NACIMIENTO'];
  			$ci=$value['CI'];
  			$celular=$value['NRO_CELULAR'];
  			$rol=$value['ROL'];
  			$correo=$value['CORREO'];
  			$passw=$value['CONTRASEÑA'];
  			$status=$value['ESTADO'];
  		}
  	?>
  	<h1>REGISTRO DEL EDITOR: <?php echo strtoupper($nombres." ".$paterno." ".$materno); ?></h1>
    <table class="tabla1" align="center">
      <form action="../Procesos/update.php" method="POST">
      <tr>
        <td>
        
            
          <table class="tabla2">
            <caption>DATOS PERSONALES</caption> 
            <tr>
            	<td>ID USUARIO</td>
            	<?php echo "<td><input type='text' value= $id_user  name='id_user' readonly required></td>"; ?>
            </tr>          
            <tr>
            	<td>NOMBRES</td>
              <?php echo "<td><input type='text' value= $nombres  name='nombre' readonly required></td>"; ?>
            </tr>
            <tr>
            	<td>PATERNO</td>
              <?php echo "<td><input type='text' value= $paterno  name='paterno' readonly required></td>"; ?>
            </tr>
            <tr>
            	<td>MATERNO</td>
            	<?php echo "<td><input type='text' value= $materno  name='materno' readonly required></td>"; ?>
            </tr>
            <tr>
            	<td>FECHA NACIMIENTO</td>
            	<?php echo "<td><input type='text' value= $nacimiento  name='fecha' readonly required></td>"; ?>
         
            </tr>
            <tr>
            	<td>CI</td>
            	<?php echo "<td><input type='text' value= $ci  name='ci' required></td>"; ?>
            </tr>
            <tr>
            	<td>CELULAR</td>
            	<?php echo "<td><input type='text' value= $celular  name='celular' required></td>"; ?>
      
          </table>
       </td>
   </tr>
   <tr>
   	<td>
          <table class="tabla3">
            <caption>DATOS DE USUARIO</caption>
            <tr>
            	<td>ROL</td>
            	<?php echo "<td><input type='text' value= $rol  name='rol' required></td>"; ?>
            </tr>
            <tr>
            	<td>EMAIL</td>
            	<?php echo "<td><input type='email' value= $correo  name='correo' required></td>"; ?>
   
            </tr>
            <tr>
            	<td>CONTRASEÑA</td>
            	<?php echo "<td><input type='text' value= $passw  name='contraseña' required></td>"; ?>
            </tr>
            <tr>
            	<td>ESTADO</td>
            	<?php echo "<td><input type='number' value= $status  name='estado' min='0' max='1'></td>"; ?>
            </tr>
          </table>
        </td>
   </tr>
      <tr>
          <td><input type="submit" value="Actualizar"></td>
        </form>
          <td><a href="update-delete.php">Volver</a></td>
        </tr>
        
    </table>
    
  	

</body>
</html>
