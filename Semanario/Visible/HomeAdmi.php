<?php 
	session_start();
	require '../Conexion/conexion.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Menu de Navegación</title>
  <link rel="stylesheet" href="../CSS/stylehomeadmin.css">
</head>
<body>

  <header>
    <div class="menu">
      <img src="../logo.png" alt="">
      <nav>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre mi</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="../Procesos/close.php">Cerrar Sesion</a></li>
          </ul>
      </nav>
    </div>
  </header>
  <section>
  	<h1>BIENVENIDO(A) A LA PAGINA</h1>
  	 <p>
      <br>
        <?php echo strtoupper($_SESSION['user']);?>
        <br><br>  
        </p>
        <h3>ACCIONES DISPONIBLES</h3>
        <div class="button-box">
        	<button><a href="usernuevo.php">Registrar Nuevo Usuario</a></button>
        <button><a href="registro.php">Revisar Registros de Usuario</a></button>
        <button><a href="update-delete.php">Actualizar/Borrar Datos de Editores</a></button>
        </div>
        
    
  </section>

</body>
</html>
