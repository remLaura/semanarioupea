<?php 
	session_start();
	require '../Conexion/conexion.php';
  $sql="SELECT * FROM usuarios WHERE ROL='EDITOR'";
  $result=$conexion->query($sql);
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Menu de Navegación</title>
  <link rel="stylesheet" href="../CSS/styleupdatedel.css">
</head>
<body>

  <header>
    <div class="menu">
      <img src="../logo.png" alt="">
      <nav>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre mi</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="../Procesos/close.php">Cerrar Sesion</a></li>
          </ul>
      </nav>
    </div>
  </header>
  <section>
    <br><br>
     <?php
              echo "<table class='tab1'>
                    <caption>ESTADO ACTUAL DE LOS EDITORES</caption>
                <tr>
                    <th>NOMBRES</th>
                    <th>PATERNO</th>
                    <th>MATERNO</th>
                    <th>ROL</th>
                    <th>ESTADO</th>
                    <th colspan='2'>ACCION</th>
                </tr>";
              foreach ($result as $value) {
              $iduser=$value['ID_USUARIO'];
              $status=$value['ESTADO'];
              echo "<tr>
                    <td>$value[NOMBRE]</td>
                    <td>$value[PATERNO]</td>
                    <td>$value[MATERNO]</td>
                    <td>$value[ROL]</td>";
                    if ($status=='1') {
                      echo "<td>ACTIVO</td>";
                    }else{
                      echo "<td>INACTIVO</td>";
                    }
                    echo "<td><a href='actualizar.php?iduser=$iduser'>Actualizar Datos</a></td>";
                    echo "<td><a href='../Procesos/delete.php?iduser=$iduser'>Eliminar Usuario</a></td>";  
                  echo"  </tr>";
             }
        echo '</table>';
        echo "<div class='boton'><button><a href='HomeAdmi.php'>Volver</a></button></div>";
        ?>
        
        
        

  </section>

</body>
</html>
