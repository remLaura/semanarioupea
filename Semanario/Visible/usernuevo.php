<?php 
	session_start();
	require '../Conexion/conexion.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Menu de Navegación</title>
  <link rel="stylesheet" href="../CSS/stylenewuser.css">
</head>
<body>

  <header>
    <div class="menu">
      <img src="../logo.png" alt="">
      <nav>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre mi</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="../Procesos/close.php">Cerrar Sesion</a></li>
          </ul>
      </nav>
    </div>
  </header>
  <section>
  	<h1>REGISTRO DE NUEVO USUARIO</h1>
    <table class="tabla1" align="center">
      <form action="../Procesos/registranuevo.php" method="POST">
      <tr>
        <td>
          <table class="tabla2">
            <caption>DATOS PERSONALES</caption>
            
            <tr>
              <td colspan="2"><input type="text" placeholder="Nombres" name="nombre" required></td>
            </tr>
            <tr>
              <td><input type="text" placeholder="Paterno" name="paterno"required></td>
              <td><input type="text" placeholder="Materno" name="materno" required></td>
            </tr>
            <tr>
              <td><input type="text" placeholder="Fecha Nacimiento (A-M-D)" name="fecha" required></td>
              <td><input type="text" placeholder="CI" name="ci" required></td>
            </tr>
            <tr>
              <td><input type="text" placeholder="Nro Celular" name="celular" required></td>
          </table>
       </td>
       <td class="t3">
          <table class="tabla3">
            <caption>DATOS DE USUARIO</caption>
            <tr>
              <td><input type="text" placeholder="Rol" name="rol" required></td>
            </tr>
            <tr>
              <td><input type="email" placeholder="Correo" name="correo" required></td>
            </tr>
            <tr>
              <td><input type="text" placeholder="Contraseña" name="passw" required></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
          <td><input type="submit" value="Registrar"></td>
        </form>
          <td class="tf"><button><a href="HomeAdmi.php">Volver</a></button></td>
        </tr>
        
    </table>
    
  	

</body>
</html>
