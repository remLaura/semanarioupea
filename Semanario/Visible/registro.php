<?php 
	session_start();
	require '../Conexion/conexion.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Menu de Navegación</title>
  <link rel="stylesheet" href="../CSS/styleregistros.css">
</head>
<body>

  <header>
    <div class="menu">
      <img src="../logo.png" alt="">
      <nav>
          <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre mi</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="../Procesos/close.php">Cerrar Sesion</a></li>
          </ul>
      </nav>
    </div>
  </header>
  <section>
      <br><br>
        <?php 
              $sql="SELECT * FROM usuarios";
              $result=$conexion->query($sql);
              echo "<table class='tab1'>
                    <caption>LISTA DE USUARIOS REGISTRADOS</caption>
                <tr>
                    <th>NOMBRES</th>
                    <th>PATERNO</th>
                    <th>MATERNO</th>
                    <th>ROL</th>
                    <th>FECHA DE NACIMIENTO</th>
                    <th>CI</th>
                    <th>CELULAR</th>
                    <th>ESTADO</th>
                </tr>";
              foreach ($result as $value) {
              $status=$value['ESTADO'];
              echo "<tr>";
                    echo "<td>$value[NOMBRE]</td>";
                    echo "<td>$value[PATERNO]</td>";
                    echo "<td>$value[MATERNO]</td>";
                    echo "<td>$value[ROL]</td>";
                    echo '<td>'.$value["FECHA_NACIMIENTO"].'</td>';
                    echo '<td>'.$value["CI"].'</td>';
                    echo '<td>'.$value["NRO_CELULAR"].'</td>';
                    if ($status=="1") {
                      echo "<td>ACTIVO</td>";
                    }else{
                      echo "<td>INACTIVO</td>";
                    }

                  echo"  </tr>";
             }
        echo '</table>';

        echo "<div class='boton'><button><a href='HomeAdmi.php'>Volver</a></button></div>";
        ?>
        
        

  </section>

</body>
</html>
